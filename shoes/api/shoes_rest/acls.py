import json
import requests
import os
from .models import Shoe

def get_shoe_pic_url(shoe):
    urls = ['https://images.saymedia-content.com/.image/c_limit%2Ccs_srgb%2Cq_auto:eco%2Cw_500/MTc0NTAxMTE2NjcwMzIyNjc4/25-weird-shoes-of-the-internet.webp',
            'https://images.saymedia-content.com/.image/c_limit%2Ccs_srgb%2Cq_auto:eco%2Cw_384/MTc0NTAxMTE2OTM4Mjk5Mzgy/25-weird-shoes-of-the-internet.webp',
            'https://images.saymedia-content.com/.image/c_limit%2Ccs_srgb%2Cq_auto:eco%2Cw_512/MTc0NTAxMTE2NjY5OTI5NDYy/25-weird-shoes-of-the-internet.webp',
            ]
    i = Shoe.objects.count() % 3
    return {'pic_url': urls[i]}