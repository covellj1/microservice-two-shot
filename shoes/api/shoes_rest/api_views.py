from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Shoe, BinVO
from common.json import ModelEncoder
from .acls import get_shoe_pic_url
import json

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["bin_number", "closet_name", "import_href"]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["id", "manufacturer", "model", "color", "pic_url"]

    def get_extra_data(self,o):
        return {"bin_number": o.bin.bin_number,
                "closet_name": o.bin.closet_name}

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = ["manufacturer", "model", "color", "pic_url", "bin"]
    encoders = {
        "bin": BinVODetailEncoder()
    }


# Create your views here.
@require_http_methods(["GET", "POST"])
def api_list_shoes(request, binVO_id=None):
    if request.method == "GET":
        if binVO_id is not None:
            shoes = Shoe.objects.filter(bin=binVO_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse({"shoes":shoes}, encoder=ShoeListEncoder)
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse({"message": "Invalid Bin id"}, status=400)

        if not content.get('pic_url'):
            pic_url = get_shoe_pic_url(content)
            content.update(pic_url)
        shoe = Shoe.objects.create(**content)
        return JsonResponse(shoe,encoder=ShoeDetailEncoder,safe=False)

@require_http_methods(["GET", "PUT", "DELETE"])
def api_shoe_details(request, pk):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(shoe,encoder=ShoeDetailEncoder,safe=False)
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count>0})
    else:
        content = json.loads(request.body)
        Shoe.objects.filter(id=pk).update(**content)
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(shoe,encoder=ShoeDetailEncoder,safe=False)
