from django.db import models

# Create your models here.
class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200,unique=True)


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200)
    model = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    bin = models.ForeignKey(BinVO, related_name="shoes", on_delete=models.CASCADE)
    pic_url = models.URLField()
