# Generated by Django 4.0.3 on 2023-12-13 23:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0004_alter_shoe_bin'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shoe',
            name='bin',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='shoes', to='shoes_rest.binvo'),
            preserve_default=False,
        ),
    ]
