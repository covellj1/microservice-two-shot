from django.db import models

# Create your models here.

class LocationVO(models.Model):
    closet_name = models.CharField(max_length=255)
    section_number = models.PositiveSmallIntegerField(null=True)
    shelf_number = models.PositiveSmallIntegerField(null=True)
    import_href = models.CharField(max_length=255, unique=True)

class Hat(models.Model):
    fabric = models.CharField(max_length=255)
    style_name = models.CharField(max_length=255)
    color = models.CharField(max_length=255)
    picture_url = models.URLField(null=True, blank=True)

    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.style_name
