# Wardrobify

Team:

* Joshua Covell - shoes
* John Romua - hats
## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

Models:
    Shoe:
        Has a manufacturer, model, color
        Is located inside an instance of a Bin within the wardobe microservice
        get assigned a picture url pulled from pexels api

    BinVO:
        Has a bin number and href assoctiated with it that are polled from the wardrobe microservice

Shoes microservice polls the wardrobe every minute to update the Bins data

## Hats microservice

The Hat model used for the hats microservice includes the following fields: fabric, style name, color, image, and the location of the hat. The LocationVO model points to the location that the hat is stored and is connected to the wardrobe. This also checks the location details to update the wardrobe via poller.
