import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';
import HatList from './HatList';
import HatForm from './HatForm';
import { useState, useEffect } from 'react';

function App() {
  const [shoes, setShoes] = useState([])
  const [hats, setHats] = useState([]);

  async function getShoes() {
    const response = await fetch('http://localhost:8080/api/shoes/');
    if (response.ok) {
      const { shoes } = await response.json();
      setShoes(shoes);
    } else {
      console.error(response);
    }
  }

  async function getHats() {
    const response = await fetch('http://localhost:8090/api/hats/');
    if (response.ok) {
      const { hats } = await response.json();
      setHats(hats);
    } else {
      console.error(response);
    }
  }

  useEffect(() => {
    getShoes();
    getHats();
  },[])

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route index element={<ShoeList shoes={shoes} getShoes={getShoes}/>} />
            <Route path="new" element={<ShoeForm getShoes={getShoes}/>} />
          </Route>

          <Route path="hats">
            <Route index element={<HatList hats={hats} getHats={getHats}/>} />
            <Route path="new" element={<HatForm getHats={getHats}/>} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
