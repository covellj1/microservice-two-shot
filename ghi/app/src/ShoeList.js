import { NavLink } from 'react-router-dom';

const ShoeList = ({shoes, getShoes}) => {
    const handleDelete = async (event) => {
        event.preventDefault();
        const shoe_id = event.target.value;
        const url = `http://localhost:8080/api/shoes/${shoe_id}`;
        const fetchConfig = {
            method: "delete",
            headers: {'Content-Type': 'application/json'}
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            getShoes();
        }
    };
    if (shoes.length===0) {
        return (
            <div className="px-4 py-5 my-5 text-center">
                <h1 className="display-5 fw-bold">No Shoes in Wardrobe :(</h1>
                <div className="col-lg-6 mx-auto">
                    <p className="lead mb-4">
                    <NavLink to="/shoes/new" className="btn btn-primary btn-lg px-4 gap-3">Add New Shoes</NavLink>
                    </p>
                </div>
            </div>
        )
    }
    return (
        <>
            <div className="row">
                {shoes.map(shoe => {
                    return (
                        <div className="col-3" key={shoe.id} >
                            <div className="card mb-5 shadow">
                                <img src={shoe.pic_url} className="card-img-top" />
                                <div className="card-body">
                                    <h4 className="card-title mb-3">{shoe.manufacturer}</h4>
                                    <h5 className="card-subtitle mb-3">{shoe.model}</h5>
                                    <h6 className="card-subtitle mb-3">{shoe.color}</h6>
                                    <h6 className="card-subtitle text-muted">Closet: {shoe.closet_name}</h6>
                                    <h6 className="card-subtitle text-muted">Bin: {shoe.bin_number}</h6>
                                </div>
                                <div className="card-footer text-center">
                                    <button className="btn btn-primary" onClick={handleDelete} value={shoe.id}>Delete Shoe</button>
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>
        </>
    );
}

export default ShoeList;
