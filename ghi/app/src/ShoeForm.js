import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

function ShoeForm({getShoes}){
    const navigate = useNavigate();
    const [bins, setBins] = useState([]);
    const[formData, setFormData] = useState({
        manufacturer: '',
        model: '',
        color:'',
        pic_url: '',
        bin: ''
    });

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8080/api/shoes/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(url,fetchConfig);

        if (response.ok) {
            setFormData({
                manufacturer: '',
                model: '',
                color:'',
                pic_url: '',
                bin: ''
            });
            getShoes();
            navigate("/shoes")
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add New Shoe to Wardobe</h1>
                    <form onSubmit={handleSubmit} id="create-shoe-form">

                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" value={formData.manufacturer}/>
                        <label htmlFor="manufacturer">Manufacturer</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="Model" required type="text" name="model" id="model" className="form-control" value={formData.model}/>
                        <label htmlFor="model">Model</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={formData.color}/>
                        <label htmlFor="color">Color</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="Picture URL (optional)" type="url" name="pic_url" id="pic_url" className="form-control" value={formData.pic_url}/>
                        <label htmlFor="pic_url">Picture URL (optional)</label>
                    </div>

                    <div className="mb-3">
                        <select onChange={handleFormChange} required name="bin" id="bin" className="form-select" value={formData.bin}>
                        <option value="">Choose a Bin</option>
                        {bins.map(bin => {
                            return (
                            <option key={bin.href} value={bin.href}>{bin.closet_name} - Bin: {bin.bin_number}</option>
                            )
                        })}
                        </select>
                    </div>
                        <button className="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default ShoeForm;
