import React from 'react';
import { Link } from 'react-router-dom';

// Fixed and updated HatList, thanks to the help and explanation from Josh
const HatList = ({hats, getHats}) => {
    const deleteHat = async (event) => {
        event.preventDefault();
        const hat_id = event.target.value;
        const url = `http://localhost:8090/api/hats/${hat_id}`;
        const fetchConfig = {
            method: "delete",
            headers: {'Content-Type': 'application/json'}
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            getHats();
        }
    };
    if (hats.length===0) {
        return (
            <div className="px-4 py-5 my-5 text-center">
                <h1 className="display-5 fw-bold">No Hats in Wardrobe</h1>
                <div className="col-lg-6 mx-auto">
                    <p className="lead mb-4">
                    <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Add New Hats</Link>
                    </p>
                </div>
            </div>
        )
    }
    return (
        <>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>Fabric</th>
                        <th>Style name</th>
                        <th>Color</th>
                        <th>Picture</th>
                        <th>Location</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {hats.map(hat => {
                        return (
                            <tr key={hat.id}>
                                <td>{ hat.fabric }</td>
                                <td>{ hat.style_name }</td>
                                <td>{ hat.color }</td>
                                <td><img src={ hat.picture_url } alt='' width="17%" height="17%" /></td>
                                <td>{ hat.location }</td>
                                <td>
                                    <button className="btn btn-danger" onClick={deleteHat} value={hat.id}>Delete Hat</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
}

export default HatList;
